import faker from 'faker'

import * as Constants from '../../src/constants'
import makeSut from '../../src/services/glpiUser.service'

import { makeFake, makeFakeError, makeFakeRejection } from '../mocks/axios.mock'
import { AxiosInstance } from 'axios'
import helmet from 'helmet'

describe('GLPi User Service', () => {
  describe('Smoke', () => {
    const sut = makeSut(faker.lorem.word())

    it('should be a factory', () => {
      expect(makeSut).toBeDefined()
      expect(makeSut).toBeInstanceOf(Function)
      expect(sut).toBeInstanceOf(Object)
    })

    it('should define getIdByEmail', () => {
      expect(sut.getIdByEmail).toBeDefined()
      expect(sut.getIdByEmail).toBeInstanceOf(Function)
    })

    it('should define create method', () => {
      expect(sut.create).toBeDefined()
      expect(sut.create).toBeInstanceOf(Function)
    })

    it('should has default params to arguments except sessionToken', () => {
      expect(() => makeSut(faker.lorem.word())).not.toThrow()
    })
  })

  describe('Methods', () => {
    const fakeGet = makeFake(200, {
      count: 1,
      data: [{ '2': faker.random.number() }]
    })
    const fakePost = makeFake(201, { id: faker.random.number() })
    function makeAxiosMock(): Partial<AxiosInstance> {
      return { get: fakeGet, post: fakePost }
    }

    const sessionToken = faker.lorem.word()
    let axios = makeAxiosMock() as AxiosInstance
    let sut = makeSut(sessionToken, axios)

    beforeEach(() => {
      axios = makeAxiosMock() as AxiosInstance
      sut = makeSut(sessionToken, axios)
    })

    describe('getIdByEmail - Get user id using email as search param', () => {
      it('should return User ID as a number', async () => {
        const received = await sut.getIdByEmail(faker.internet.email())
        expect(received).toBeDefined()
        expect(typeof received).toBe('number')
      })

      it('should call $http.get to find a user id using email', async () => {
        await sut.getIdByEmail(faker.internet.email())
        expect(fakeGet).toHaveBeenCalled()
      })

      it('should call $http.get to with a correct endpoint (/search/User)', async () => {
        await sut.getIdByEmail(faker.internet.email())
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(expect.arrayContaining(['/search/User']))
      })

      it('should call $http.get with some params', async () => {
        const email = faker.internet.email()
        await sut.getIdByEmail(email)
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              params: {
                'criteria[0][link]': 'AND',
                'criteria[0][field]': '5',
                'criteria[0][searchtype]': 'contains',
                'criteria[0][value]': email,
                'forcedisplay[0]': Constants.userIdKey
              }
            })
          ])
        )
      })

      it('should use sessionToken on request headers', async () => {
        await sut.getIdByEmail(faker.internet.email())
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              headers: { 'Session-Token': sessionToken }
            })
          ])
        )
      })

      it('should throw when request fails', async () => {
        axios.get = makeFakeRejection()
        try {
          await sut.getIdByEmail(faker.internet.email())
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.searchUserIdFail))
        }
      })

      it('should throw when get status !== 200', async () => {
        axios.get = makeFakeError()
        try {
          await sut.getIdByEmail(faker.internet.email())
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.searchUserIdFail))
        }
      })

      it('should throw when get empty data', async () => {
        axios.get = makeFake(200, {})
        try {
          await sut.getIdByEmail(faker.internet.email())
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.searchUserIdFail))
        }
      })

      it('should throw when count prop was 0', async () => {
        axios.get = makeFake(200, { count: 0 })
        try {
          await sut.getIdByEmail(faker.internet.email())
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.searchUserIdFail))
        }
      })
    })

    describe('create - Add new user', () => {
      it('should return User ID as a number', async () => {
        const received = await sut.create(
          faker.internet.email(),
          faker.name.findName(),
          faker.phone.phoneNumber()
        )

        expect(received).toBeDefined()
        expect(typeof received).toBe('number')
      })

      it('should use email as username', async () => {
        const email = faker.internet.email()
        await sut.create(
          email,
          faker.name.findName(),
          faker.phone.phoneNumber()
        )

        const [params] = fakePost.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            {
              input: expect.objectContaining({ name: email })
            }
          ])
        )
      })

      it('should clean initial space at realname after split it', async () => {
        const name = faker.name.findName()
        const realname = name.split(' ').slice(1).join(' ')

        await sut.create(
          faker.internet.email(),
          name,
          faker.phone.phoneNumber()
        )
        const [params] = fakePost.mock.calls

        expect(realname.charAt(0)).not.toBe(' ')
        expect(params).toEqual(
          expect.arrayContaining([
            {
              input: expect.objectContaining({ realname })
            }
          ])
        )
      })

      it('should put phone into new user payload', async () => {
        const phone = faker.phone.phoneNumber()

        await sut.create(faker.internet.email(), faker.name.findName(), phone)
        const [params] = fakePost.mock.calls

        expect(params).toEqual(
          expect.arrayContaining([
            {
              input: expect.objectContaining({ phone })
            }
          ])
        )
      })

      it('should throw when request fails', async () => {
        axios.post = makeFakeRejection()
        try {
          await sut.create(
            faker.internet.email(),
            faker.name.findName(),
            faker.phone.phoneNumber()
          )
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.createUserFail))
        }
      })

      it('should throw when get status !== 201', async () => {
        axios.post = makeFakeError()
        try {
          await sut.create(
            faker.internet.email(),
            faker.name.findName(),
            faker.phone.phoneNumber()
          )
          throw new Error()
        } catch (e) {
          expect(e).toEqual(Error(Constants.createUserFail))
        }
      })
    })
  })
})
