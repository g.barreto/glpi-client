import faker from 'faker'

import makeSut from '../../src/services/glpiSession.service'
import * as Constants from '../../src/constants'

import { makeFake, makeFakeError, makeFakeRejection } from '../mocks/axios.mock'
import { AxiosInstance } from 'axios'

describe('GLPi Session Service', () => {
  describe('Smoke', () => {
    const sut = makeSut()

    it('should be a factory', () => {
      expect(makeSut).toBeInstanceOf(Function)
      expect(sut).toBeDefined()
      expect(sut).toBeInstanceOf(Object)
    })

    it('should define init method', () => {
      expect(sut.init).toBeDefined()
      expect(sut.init).toBeInstanceOf(Function)
    })

    it('should have default params', () => {
      expect(() => makeSut()).not.toThrow()
    })
  })

  describe('init - Create a Session and return session token', () => {
    const token = faker.lorem.word()
    const fakeGet = makeFake(200, { session_token: token })
    function makeAxiosMock(): Partial<AxiosInstance> {
      return {
        get: fakeGet
      }
    }

    let axios = makeAxiosMock() as AxiosInstance
    let sut = makeSut(axios)

    beforeEach(() => {
      axios = makeAxiosMock() as AxiosInstance
      sut = makeSut(axios)
    })

    it('should return a sessionToken as an string', async () => {
      const sessionToken = await sut.init()
      expect(sessionToken).toBeDefined()
      expect(typeof sessionToken).toBe('string')
    })

    it('should call $http.get to retrieve sessionToken', async () => {
      await sut.init()
      expect(fakeGet).toHaveBeenCalled()
    })

    it('should call $http.get with correct endpoint (/initSession)', async () => {
      await sut.init()
      const [params] = fakeGet.mock.calls
      expect(params).toEqual(expect.arrayContaining(['/initSession']))
    })

    it('should throw when get status !== 200', async () => {
      axios.get = makeFakeError({})
      try {
        await sut.init()
      } catch (ex) {
        expect(ex).toEqual(Error(Constants.initSessionFail))
      }
    })

    it('should throw when request fails', async () => {
      axios.get = makeFakeRejection()
      try {
        await sut.init()
      } catch (ex) {
        expect(ex).toEqual(Error(Constants.initSessionFail))
      }
    })
  })
})
