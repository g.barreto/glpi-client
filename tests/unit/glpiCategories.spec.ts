import faker from 'faker'

import config from '../../src/config'
import * as Constants from '../../src/constants'
import makeSut from '../../src/services/glpiCategories.service'

import { makeFake, makeFakeRejection, makeFakeError } from '../mocks/axios.mock'
import { AxiosInstance } from 'axios'

function makeFakeCategories(
  length = faker.random.number({ min: 32, max: 256 })
) {
  return Array.from({ length }, () => ({
    id: faker.random.number(),
    name: faker.lorem.word(),
    completename: faker.lorem.words().split(' ').join(' > ')
  }))
}

describe('GLPi Categories Service', () => {
  const sessionToken = faker.random.uuid()

  describe('Smoke', () => {
    const sut = makeSut(sessionToken)

    it('should be a factory', () => {
      expect(makeSut).toBeInstanceOf(Function)
      expect(sut).toBeDefined()
      expect(sut).toBeInstanceOf(Object)
    })

    it('should define fetchAll method', () => {
      expect(sut.fetchAll).toBeDefined()
      expect(sut.fetchAll).toBeInstanceOf(Function)
    })

    it('should has default params except to session token', () => {
      expect(() => makeSut(sessionToken)).not.toThrow()
    })
  })

  describe('Methods', () => {
    const fakeRange = faker.lorem.word()
    const fakeData = makeFakeCategories()
    const fakeGet = makeFake(200, fakeData)
    function makeAxiosMock(): Partial<AxiosInstance> {
      return {
        get: fakeGet
      }
    }

    let axios = makeAxiosMock() as AxiosInstance
    let sut = makeSut(sessionToken, axios)

    beforeAll(() => {
      config.glpi.categoriesRange = fakeRange
    })

    beforeEach(() => {
      axios = makeAxiosMock() as AxiosInstance
      sut = makeSut(sessionToken, axios)
    })

    describe('fetchAll - Get all ticket categories', () => {
      it('should use session token on request header', async () => {
        await sut.fetchAll()
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              headers: { 'Session-Token': sessionToken }
            })
          ])
        )
      })

      it('should include categoriesRange config at request params', async () => {
        await sut.fetchAll()
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              params: expect.objectContaining({ range: fakeRange })
            })
          ])
        )
      })

      it('should return a list of object with id, name and completeName', async () => {
        const received = await sut.fetchAll({ onlyIds: false })
        const expected = ['id', 'name', 'completename']
        received.every((each) => {
          return expect(Object.keys(each)).toEqual(expected)
        })
      })

      it('should return only name when onlyNames param was true', async () => {
        const received = await sut.fetchAll({ onlyIds: false, onlyNames: true })
        expect(received).toEqual(fakeData.map(({ name }) => name))
      })

      it('should return only completeNames when completeNames was true', async () => {
        const received = await sut.fetchAll({
          onlyIds: false,
          completeNames: true
        })
        expect(received).toEqual(
          fakeData.map(({ completename }) => completename)
        )
      })

      it('should return only ids when any config was passed', async () => {
        const received = await sut.fetchAll()
        expect(received).toEqual(fakeData.map(({ id }) => id))
      })

      it(`should throw ${Constants.fetchCategoriesFail} when request fails`, async () => {
        axios.get = makeFakeRejection()
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchCategoriesFail))
        }
      })

      it(`should throw ${Constants.fetchCategoriesFail} when get status !== 200`, async () => {
        axios.get = makeFakeError()
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchCategoriesFail))
        }
      })

      it(`should throw ${Constants.fetchCategoriesFail} when get an empty array`, async () => {
        axios.get = makeFake(200, [])
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchCategoriesFail))
        }
      })
    })
  })
})
