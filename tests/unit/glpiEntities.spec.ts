import faker from 'faker'

import * as Constants from '../../src/constants'
import makeSut from '../../src/services/glpiEntities.service'

import { makeFake, makeFakeRejection, makeFakeError } from '../mocks/axios.mock'
import { AxiosInstance } from 'axios'

function makeFakeEntities(length = faker.random.number({ min: 32, max: 256 })) {
  return {
    active_entity: {
      active_entities: Array.from({ length }, () => ({
        id: faker.random.number()
      }))
    }
  }
}

describe('GLPi Entities Service', () => {
  const sessionToken = faker.random.uuid()

  describe('Smoke', () => {
    const sut = makeSut(sessionToken)

    it('should be a factory', () => {
      expect(makeSut).toBeInstanceOf(Function)
      expect(sut).toBeDefined()
      expect(sut).toBeInstanceOf(Object)
    })

    it('should define fetchAll method', () => {
      expect(sut.fetchAll).toBeDefined()
      expect(sut.fetchAll).toBeInstanceOf(Function)
    })

    it('should has default params except to session token', () => {
      expect(() => makeSut(sessionToken)).not.toThrow()
    })
  })

  describe('Methods', () => {
    const fakeData = makeFakeEntities()
    const fakeGet = makeFake(200, fakeData)
    function makeAxiosMock(): Partial<AxiosInstance> {
      return {
        get: fakeGet
      }
    }

    let axios = makeAxiosMock() as AxiosInstance
    let sut = makeSut(sessionToken, axios)

    beforeEach(() => {
      axios = makeAxiosMock() as AxiosInstance
      sut = makeSut(sessionToken, axios)
    })

    describe('fetchAll - Get all active entities', () => {
      it('should call correct endpoint (/getActiveEntities)', async () => {
        await sut.fetchAll()
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(expect.arrayContaining(['/getActiveEntities']))
      })

      it('should use session token on request header', async () => {
        await sut.fetchAll()
        const [params] = fakeGet.mock.calls
        expect(params).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              headers: { 'Session-Token': sessionToken }
            })
          ])
        )
      })

      it('should return a list of ids', async () => {
        const fakeIds = fakeData.active_entity.active_entities.map(
          ({ id }) => id
        )
        const received = await sut.fetchAll()
        received.every((each) => {
          expect(typeof each).toBe('number')
          expect(fakeIds).toContain(each)
        })
      })

      it(`should throw ${Constants.fetchEntitiesFail} when request fails`, async () => {
        axios.get = makeFakeRejection()
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchEntitiesFail))
        }
      })

      it(`should throw ${Constants.fetchCategoriesFail} when get status !== 200`, async () => {
        axios.get = makeFakeError()
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchEntitiesFail))
        }
      })

      it(`should throw ${Constants.fetchEntitiesFail} when get an empty array`, async () => {
        axios.get = makeFake(200, makeFakeEntities(0))
        try {
          await sut.fetchAll()
        } catch (e) {
          expect(e).toEqual(Error(Constants.fetchEntitiesFail))
        }
      })
    })
  })
})
