import faker, { fake } from 'faker'

const errorStatusCode = faker.random.number({ min: 400, max: 500 })

export const makeFake = (
  status: number,
  data: Record<string, any> = {}
): jest.Mock<any, any> => jest.fn().mockResolvedValue({ status, data })

type Response = {
  status: number
  data: Record<string, any>
}
export const makeMultipleFakes = (calls: Response[]): jest.Mock<any, any> => {
  const mock = jest.fn()
  calls.forEach((each) => mock.mockResolvedValueOnce(each))
  return mock
}

export const makeFakeRejection = (): jest.Mock<any, any> =>
  jest.fn().mockRejectedValue({ status: errorStatusCode, data: {} })

export const makeFakeError = (data = {}): jest.Mock<any, any> =>
  makeFake(errorStatusCode, data)

export const makeFakeErrors = (times = 1): jest.Mock<any, any> =>
  makeMultipleFakes(
    Array.from({ length: times }, () => ({
      status: errorStatusCode,
      data: {}
    }))
  )
