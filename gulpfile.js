/* eslint-disable @typescript-eslint/no-var-requires */
const { dest, series, src } = require('gulp')
const ts = require('gulp-typescript')
const rimraf = require('rimraf')

const tsProject = ts.createProject('tsconfig.json')

function clean(cb) {
  rimraf('dist', cb)
}

function buildTs(cb) {
  src('src/**/*.ts').pipe(tsProject()).pipe(dest('dist'))
  cb()
}

exports.default = series(clean, buildTs)
