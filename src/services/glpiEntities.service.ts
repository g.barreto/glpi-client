import * as constants from '../constants'
import config from '../config'

import $http from './http.service'
import makeGLPiSessionHeader from './glpiSessionHeader.service'

type GLPiEntityService = {
  fetchAll: () => Promise<number[]>
}

export function makeGLPiCategoriesService(
  sessionToken: string,
  http = $http,
  glpiSessionHeaderService = makeGLPiSessionHeader,
  Constants = constants,
  Config = config
): GLPiEntityService {
  const headers = glpiSessionHeaderService(sessionToken)
  const params = {
    expand_dropdowns: true,
    range: Config.glpi.categoriesRange
  }

  async function fetchAll() {
    try {
      const res = await http.get('/getActiveEntities', { headers, params })
      if (res.status !== 200) throw new Error()

      const { active_entities: entity } = res.data.active_entity
      if (!entity || !entity.length) throw new Error()

      return entity.map(({ id }) => id)
    } catch (ex) {
      throw new Error(Constants.fetchEntitiesFail)
    }
  }

  return { fetchAll }
}

export default makeGLPiCategoriesService
