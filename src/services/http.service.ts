import axios from 'axios'

import config from '../config'

export default axios.create({
  baseURL: config.glpi.baseURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'App-Token': config.glpi.appToken,
    Authorization: `user_token ${config.glpi.userAuthorization}`
  }
})
