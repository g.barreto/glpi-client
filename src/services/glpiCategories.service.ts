import * as constants from '../constants'
import config from '../config'

import $http from './http.service'
import makeGLPiSessionHeader from './glpiSessionHeader.service'

type GLPiITILCategory = { id: number; name: string; completeName: string }

type FetchAllParams = {
  onlyIds?: boolean
  onlyNames?: boolean
  completeNames?: boolean
}

type GLPiCategoriesService = {
  fetchAll: (
    params?: FetchAllParams
  ) => Promise<(GLPiITILCategory | number | string)[]>
}

export function makeGLPiCategoriesService(
  sessionToken: string,
  http = $http,
  glpiSessionHeaderService = makeGLPiSessionHeader,
  Constants = constants,
  Config = config
): GLPiCategoriesService {
  const headers = glpiSessionHeaderService(sessionToken)
  const params = {
    expand_dropdowns: true,
    range: Config.glpi.categoriesRange
  }

  async function fetchAll({
    onlyIds = true,
    onlyNames = false,
    completeNames = false
  } = {}) {
    try {
      const res = await http.get('/ITILCategory', { headers, params })
      if (res.status !== 200) throw new Error()

      if (onlyIds) return res.data.map(({ id }) => id)
      if (completeNames) return res.data.map(({ completename }) => completename)
      if (onlyNames) return res.data.map(({ name }) => name)

      return res.data.map(({ id, name, completename }) => ({
        id,
        name,
        completename
      }))
    } catch (ex) {
      throw new Error(Constants.fetchCategoriesFail)
    }
  }

  return { fetchAll }
}

export default makeGLPiCategoriesService
