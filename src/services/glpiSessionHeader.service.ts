type SessionHeader = { 'Session-Token': string }

export function makeGLPiSessionHeader(sessionToken: string): SessionHeader {
  const headers = { 'Session-Token': sessionToken }
  return headers
}

export default makeGLPiSessionHeader
