import * as constants from '../constants'

import $http from './http.service'

type GLPiSessionService = {
  init: () => Promise<string>
}

export function makeGLPiSessionService(
  http = $http,
  Constants = constants
): GLPiSessionService {
  async function init() {
    try {
      const authRes = await http.get('/initSession')
      if (authRes.status !== 200) throw new Error(Constants.initSessionFail)
      return authRes.data.session_token
    } catch (ex) {
      throw new Error(Constants.initSessionFail)
    }
  }

  return { init }
}

export default makeGLPiSessionService
