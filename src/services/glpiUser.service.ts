import { randomBytes } from 'crypto'

import * as $constants from '../constants'
import { makeGLPiSessionHeader } from './glpiSessionHeader.service'
import $http from './http.service'

type GLPiUserService = {
  create: (email: string, name: string, phone: string) => Promise<number>
  getIdByEmail: (email: string) => Promise<number | null>
}

export function makeGLPiUserService(
  sessionToken: string,
  http = $http,
  Constants = $constants,
  glpiSessionHeaderService = makeGLPiSessionHeader
): GLPiUserService {
  const headers = glpiSessionHeaderService(sessionToken)

  function mountSearchQuery(email) {
    return {
      'criteria[0][link]': 'AND',
      'criteria[0][field]': '5',
      'criteria[0][searchtype]': 'contains',
      'criteria[0][value]': email,
      'forcedisplay[0]': Constants.userIdKey
    }
  }

  function mountNewUserPayload(email: string, name: string, phone: string) {
    const [firstname] = name.split(' ')
    const realname = (() => name.replace(`${firstname} `, ''))()
    return {
      input: {
        firstname,
        phone,
        realname: realname,
        name: email,
        password: randomBytes(64).toString('hex'),
        language: 'pt_BR',
        _useremails: [email],
        _default_email: 0,
        is_active: 1,
        entities_id: 3,
        is_default: 1,
        is_dynamic: 0
      }
    }
  }

  async function create(
    email: string,
    name: string,
    phone: string
  ): Promise<number> {
    const payload = mountNewUserPayload(email, name, phone)

    try {
      const { status, data } = await http.post('/User', payload, { headers })
      if (status !== 201) throw new Error()

      return data.id
    } catch (_) {
      throw new Error(Constants.createUserFail)
    }
  }

  async function getIdByEmail(email: string): Promise<number | null> {
    const params = mountSearchQuery(email)
    try {
      const res = await http.get('/search/User', { params, headers })
      if (res.status !== 200) throw new Error()
      if (res.data.count === 0) throw new Error()

      const { data } = res.data
      return data[0][Constants.userIdKey]
    } catch (_) {
      throw new Error(Constants.searchUserIdFail)
    }
  }

  return { create, getIdByEmail }
}

export default makeGLPiUserService
