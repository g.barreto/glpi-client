import * as $constants from '../constants'

import $http from './http.service'
import { makeGLPiSessionHeader } from './glpiSessionHeader.service'

type TicketPayload = {
  content: string
  entity: number
  title: string
  type: number
  userId: number
  category?: number
  requestType?: number
  status?: number
  urgency?: number
}
type GLPiTicketService = (
  payload: TicketPayload
) => {
  create: () => Promise<number>
}

export function makeGLPiTicketService(
  sessionToken: string,
  http = $http,
  Constants = $constants,
  glpiSessionHeaderService = makeGLPiSessionHeader
): GLPiTicketService {
  const headers = glpiSessionHeaderService(sessionToken)

  return ({
    content,
    entity,
    title,
    userId,
    category = 0,
    requestType = 2,
    status = 1,
    type = 1,
    urgency = 3
  }) => {
    function mountTicketPayload() {
      return {
        input: {
          content,
          status,
          type,
          urgency,

          name: title,
          entities_id: entity,
          itilcategories_id: category,
          requesttype_id: requestType,

          [Constants.userRequesterKey]: userId
        }
      }
    }
    async function create() {
      const body = mountTicketPayload()

      try {
        const res = await http.post('/Ticket', body, { headers })
        if (res.status !== 201) throw new Error(Constants.createTicketFail)
        return Number(res.data.id)
      } catch (ex) {
        throw new Error(Constants.createTicketFail)
      }
    }

    return { create }
  }
}

export default makeGLPiTicketService
