import * as yup from 'yup'

const $schema = yup.object().shape({
  category: yup.number(),
  content: yup.string().required(),
  email: yup.string().email().required(),
  entity: yup.number(),
  name: yup.string().required(),
  phone: yup.string().required(),
  requestType: yup.number(),
  title: yup.string().required(),
  type: yup.number(),
  urgency: yup.number(),
  status: yup.number()
})

type Validator = {
  validate: (
    payload: Record<string, any>
  ) => Promise<[boolean, null | string[]]>
}
export function makeValidator(schema = $schema): Validator {
  async function validate(
    payload: Record<string, any>
  ): Promise<[boolean, null | string[]]> {
    const isPayloadValid = await schema.isValid(payload)
    if (isPayloadValid) return [true, null]

    try {
      await $schema.validate(payload, { abortEarly: false })
      return [true, null]
    } catch (ex) {
      if (ex instanceof yup.ValidationError) {
        const { errors } = ex
        return [false, errors]
      }
      return [false, ex]
    }
  }

  return { validate }
}

export default makeValidator
