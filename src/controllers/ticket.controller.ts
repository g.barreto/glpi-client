import { Request, Response } from 'express'

import * as constants from '../constants'
import { responses } from '../errors'
import { makeValidator } from '../validators/ticket.validator'
import * as $GLPi from '../services/glpi.service'

type Controller = {
  store: (req: Request, res: Response) => Promise<Response<any>>
}

export function makeTicketController(
  validator = makeValidator(),
  GLPi = $GLPi,
  Errors = responses,
  Constants = constants
): Controller {
  async function getUserId(sessionToken, name, email, phone) {
    try {
      return await GLPi.User(sessionToken).getIdByEmail(email)
    } catch (ex) {
      if (ex.message === Constants.searchUserIdFail) {
        return await GLPi.User(sessionToken).create(email, name, phone)
      }
      throw ex
    }
  }

  async function checkCategory(sessionToken, id: number) {
    if (!id) return true

    const validCategories = await GLPi.Categories(sessionToken).fetchAll()
    if (!validCategories.includes(id))
      throw new Error(Constants.invalidCategory)

    return true
  }

  async function checkEntity(sessionToken, id: number) {
    if (!id) return true

    const validCategories = await GLPi.Entity(sessionToken).fetchAll()
    if (!validCategories.includes(id)) throw new Error(Constants.invalidEntity)

    return true
  }

  async function store(req: Request, res: Response) {
    const { body: payload } = req

    const [isPayloadValid, validationErrors] = await validator.validate(payload)
    if (!isPayloadValid) return res.status(400).json(validationErrors)

    const { name, email, entity, phone, category } = payload
    try {
      const sessionToken = await GLPi.Session().init()
      const userId = await getUserId(sessionToken, name, email, phone)

      await Promise.all([
        checkCategory(sessionToken, category),
        checkEntity(sessionToken, entity)
      ])

      const created = await GLPi.Ticket(sessionToken)({
        ...payload,
        userId
      }).create()

      return res.status(201).send({ id: created })
    } catch (ex) {
      if (Object.keys(Errors).includes(ex.message)) {
        const error = Errors[ex.message]
        return res.status(error.status).send(error)
      }

      throw ex
    }
  }

  return { store }
}

export default makeTicketController
