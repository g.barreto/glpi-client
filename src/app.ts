import express from 'express'
import compression from 'compression'
import cors from 'cors'
import morgan from 'morgan'
import helmet from 'helmet'

import config from './config'

type Application = {
  onListen: () => void
  app: Express.Application
}
export function makeApp(
  routes = express.Router(),
  plugins = [
    compression(),
    cors(),
    express.json(),
    helmet(),
    morgan('combined')
  ]
): Application {
  function onListen() {
    console.log('API is Online!')
    console.log(`[API] Environment: ${config.env}`)
    console.log(`[API] Port: ${config.port}`)
  }

  const app = express()

  plugins.forEach((plugin) => app.use(plugin))
  app.use(routes)

  return { app, onListen }
}

export default makeApp
