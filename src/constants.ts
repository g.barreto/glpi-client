export const createTicketFail = 'CREATE_TICKET_FAILS'
export const createUserFail = 'CREATE_USER_FAILS'
export const fetchCategoriesFail = 'FETCH_CATEGORIES_FAILS'
export const fetchEntitiesFail = 'FETCH_ENTITIES_FAILS'
export const initSessionFail = 'SESSION_INIT_FAILS'
export const searchUserIdFail = 'SEARCH_USER_ID_FAILS'

export const invalidCategory = 'INVALID_ITIL_CATEGORY'
export const invalidEntity = 'INVALID_ENTITY'

export const userIdKey = '2'
export const userRequesterKey = '_users_id_requester'
