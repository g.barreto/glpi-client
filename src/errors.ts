import * as constants from './constants'

export const responses = {
  [constants.searchUserIdFail]: {
    status: 500,
    code: constants.searchUserIdFail,
    message:
      'Ocorreu uma falha ao tentar recuperar o ID do email informado. Por favor, tente novamente mais tarde'
  },
  [constants.createTicketFail]: {
    status: 500,
    code: constants.createTicketFail,
    message:
      'Ocorreu uma falha ao tentar registrar o problema informado. Por favor, aguarde alguns instantes, verifique os dados e tente novamente.'
  },
  [constants.createUserFail]: {
    status: 500,
    code: constants.createUserFail,
    message:
      'Ocorreu uma falha ao tentar registrar o novo usuário. Por favor, aguarde alguns instantes e tente novamente.'
  },
  [constants.initSessionFail]: {
    status: 500,
    code: constants.initSessionFail,
    message:
      'Ocorreu uma falha ao tentar iniciar uma sessao segura de comunicacao com o servidor. Por favor, aguarde alguns instantes e tente novamente.'
  },
  [constants.fetchCategoriesFail]: {
    status: 500,
    code: constants.fetchCategoriesFail,
    message:
      'Ocorreu uma falha ao tentar recuperar as categorias disponíveis. Por favor, aguarde alguns instantes e tente novamente.'
  },
  [constants.invalidCategory]: {
    status: 400,
    code: constants.invalidCategory,
    message:
      'A categoria informada é inválida, por favor, verifique os dados e tente novamente.'
  },
  [constants.fetchEntitiesFail]: {
    status: 500,
    code: constants.fetchEntitiesFail,
    message:
      'Ocorreu uma falha ao tentar recuperar as entidades disponíveis. Por favor, aguarde alguns instantes e tente novamente.'
  },
  [constants.invalidEntity]: {
    status: 400,
    code: constants.invalidEntity,
    message:
      'A categoria informada é inválida ou não possui permissão para ser utilizada, por favor, verifique os dados e tente novamente.'
  }
}

export default { responses }
