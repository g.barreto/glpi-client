import http from 'http'

import makeApp from './app'
import config from './config'
import makeRouter from './router'

const api = makeApp(makeRouter())

http.createServer(api.app).listen(config.port, api.onListen)
