import dotenv from 'dotenv'

dotenv.config()

const {
  GLPI_API_URL: glpiApiUrl,
  GLPI_APP_TOKEN: glpiAppToken,
  GLPI_AUTHORIZATION: glpiAuthorization,
  GLPI_SESSION_TOKEN: glpiSessionToken,
  GLPI_CATEGORIES_RANGE: glpiCategoriesRanger,
  NODE_ENV: env = 'development',
  PORT: port = 5000
} = process.env

export default Object.freeze({
  env,
  port: Number(port),
  glpi: {
    appToken: String(glpiAppToken),
    baseURL: String(glpiApiUrl),
    categoriesRange: String(glpiCategoriesRanger),
    userAuthorization: String(glpiAuthorization),
    sessionToken: String(glpiSessionToken)
  }
})
