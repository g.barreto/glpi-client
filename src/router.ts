import express from 'express'
import Swagger from 'swagger-ui-express'

import SwaggerDoc from '../docs/swagger.json'
import { makeTicketController } from './controllers/ticket.controller'
import { makeExHandler } from './middleware/eHandler.middleware'

export function makeRouter(
  ticketController = makeTicketController(),
  eHandler = makeExHandler(),
  swagger = Swagger,
  docs = SwaggerDoc
): express.Router {
  const router = express.Router()

  router.use('/docs', swagger.serve, swagger.setup(docs))

  router.post('/', ticketController.store)

  router.use(eHandler.handle)

  return router
}

export default makeRouter
