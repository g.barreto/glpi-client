import { Request, Response, NextFunction } from 'express'

import Youch from 'youch'
import forTerminal from 'youch-terminal'

import config from '../config'

type ExHandler = {
  handle: (
    err: Error,
    req: Request,
    res: Response,
    next: NextFunction
  ) => Promise<Response>
}
export function makeExHandler(): ExHandler {
  async function handle(
    err: Error,
    req: Request,
    res: Response,
    _
  ): Promise<Response<any>> {
    const youchErr = new Youch(err, req)
    const jsonErr = await youchErr.toJSON()

    if (config.env === 'development') {
      forTerminal(jsonErr)
      return res.status(500).send(jsonErr)
    }

    return res.status(500).send('Internal Server Error')
  }

  return { handle }
}
